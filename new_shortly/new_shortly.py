# -*- coding: utf-8 -*-
#!/usr/bin/env python
# Shortly

import os
import redis
import urlparse
import MySQLdb
from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.wsgi import SharedDataMiddleware
from werkzeug.utils import redirect
from jinja2 import Environment, FileSystemLoader
from werkzeug.contrib.sessions import SessionMiddleware, \
     FilesystemSessionStore
# from werkzeug.debug import DebuggedApplication

# get debagger
import pudb


# helper functions
# 36-radix encoding
def base36_encode(number):
    assert number >= 0, 'positive integer required'
    if number == 0:
        return '0'
    base36 = []
    while number != 0:
        number, i = divmod(number, 36)
        base36.append('0123456789abcdefghijklmnopqrstuvwxyz'[i])
    return ''.join(reversed(base36))

def is_valid_url(url):
    parts = urlparse.urlparse(url)
    return parts.scheme in ('http', 'https')

def get_hostname(url):
    return urlparse.urlparse(url).netloc


class Shortly(object):
    def __init__(self, config):
        self.redis = redis.Redis(config['redis_host'], config['redis_port'])
        template_path = os.path.join(os.path.dirname(__file__), 'templates')
        self.jinja_env = Environment(loader=FileSystemLoader(template_path),
                                     autoescape=True)
        self.jinja_env.filters['hostname'] = get_hostname

        self.url_map = Map([
            Rule('/', endpoint='new_url'),
            Rule('/<short_id>', endpoint='follow_short_link'),
            Rule('/<short_id>+', endpoint='short_link_details'),
            Rule('/login', endpoint='login'),
            Rule('/logout', endpoint='logout'),
            Rule('/admin_details', endpoint='admin_details'),
        ])

        # session store
        self.session_store = FilesystemSessionStore()

        # MySQLdb connection
        try:
            conn = MySQLdb.connect(host="localhost", user="root", passwd="leavemealone", db="notes", charset='utf8')
            cursor = conn.cursor()
            query = "SELECT * FROM `users` WHERE `id`=1"
            cursor.execute(query)
            result = cursor.fetchall()
            self.admin_login = result[0][1]
            self.admin_password = result[0][2]
        except MySQLdb.Error:
            print(conn.error())

    # running block
    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)

    def wsgi_app(self, environ, start_response):
        request = Request(environ)
        response = self.dispatch_request(request)
        return response(environ, start_response)

    def dispatch_request(self, request):
        adapter = self.url_map.bind_to_environ(request.environ)
        try:
            endpoint, values = adapter.match()
            return getattr(self, 'on_' + endpoint)(request, **values)
        except NotFound, e:
            return self.error_404()
        except HTTPException, e:
            return e


    # url functions
    def on_admin_details(self, request):
        sid = request.cookies.get('the_cookie')
        if sid is None:
            return redirect('/')
        else:
            return self.render_template('admin_details.html')

    def on_login(self, request):
        if request.method == 'POST':

            login = request.form['login']
            password = request.form['password']
            if login == self.admin_login and password == self.admin_password:
                
                # set session and cookie
                request.session = self.session_store.new()
                response = redirect('/admin_details')
                response.set_cookie('the_cookie', request.session.sid)
                return response
            else: 
                login_error = 'Wrong data, try again.'
                return self.on_new_url(request, login_error)
        else:
            return redirect('/')

    def on_logout(self, request):
        # pudb.set_trace()
        if request.method == 'POST':
            # destroy session and cookie
            sid = request.cookies.get('the_cookie')
            session = self.session_store.get(sid)
            self.session_store.delete(session)

            response = redirect('/') 
            response.delete_cookie('the_cookie')
            return response

    def on_new_url(self, request, login_error = None):
        error = None
        url = ''
        if request.method == 'POST':
            if 'url' in request.form:
                url = request.form['url']
                if not is_valid_url(url):
                    error = 'Please enter a valid URL'
                else:
                    short_id = self.insert_url(url)
                    return redirect('/%s+' % short_id)

        return self.render_template('new_url.html', error=error, url=url, login_error=login_error)

    def on_follow_short_link(self, request, short_id):
        link_target = self.redis.get('url-target:' + short_id)
        if link_target is None:
            raise NotFound()
        self.redis.incr('click-count:' + short_id)
        return redirect(link_target)

    def on_short_link_details(self, request, short_id):
        link_target = self.redis.get('url-target:' + short_id)
        if link_target is None:
            raise NotFound()
        click_count = int(self.redis.get('click-count:' + short_id) or 0)
        return self.render_template('short_link_details.html',
            link_target=link_target,
            short_id=short_id,
            click_count=click_count
        )


    # helper functions
    def insert_url(self, url):
        short_id = self.redis.get('reverse-url:' + url)
        if short_id is not None:
            return short_id
        url_num = self.redis.incr('last-url-id')
        short_id = base36_encode(url_num)
        self.redis.set('url-target:' + short_id, url)
        self.redis.set('reverse-url:' + url, short_id)
        return short_id

    def render_template(self, template_name, **context):
        t = self.jinja_env.get_template(template_name)
        return Response(t.render(context), mimetype='text/html')
        
    def error_404(self):
        response = self.render_template('404.html')
        response.status_code = 404
        return r


def create_app(redis_host='localhost', redis_port=6379, with_static=True):
    app = Shortly({
        'redis_host':       redis_host,
        'redis_port':       redis_port
    })
    if with_static:
        app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
            '/static':  os.path.join(os.path.dirname(__file__), 'static')
        })
    # app.wsgi_app = DebuggedApplication(app.wsgi_app , evalex=True)
    return app


if __name__ == '__main__':
    from werkzeug.serving import run_simple
    app = create_app()
    run_simple('127.0.0.1', 5000, app, use_debugger=True, use_reloader=True)